# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="Terminal-based Text Editor with extensive Unicode and CJK support"
HOMEPAGE="http://towo.net/mined/"
SRC_URI="mirror://sourceforge/mined/${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS=""

# Really it has no deps?
DEPEND=""
RDEPEND=""

RESTRICT="mirror"