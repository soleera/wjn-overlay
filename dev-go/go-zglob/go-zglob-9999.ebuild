# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

EGO_PN="github.com/mattn/${PN}"

inherit golang-build golang-vcs

DESCRIPTION="zglob implementation in Go"
HOMEPAGE="https://github.com/mattn/go-zglob"

LICENSE="MIT"
SLOT="0"

DEPEND=""
RDEPEND=""
