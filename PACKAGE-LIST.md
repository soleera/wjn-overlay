wjn-overlay packages list
========

- This list is updated on 2017-06-04 EDT

| Package Name | Description | Upstream Website | This Overlay Only | Note |
| ---- | ---- | ---- | ---- | ---- |
| app-accessibility/florence | an extensible scalable virtual keyboard for X11 | <http://florence.sourceforge.net/> | o | |
| app-accessibility/onboard | Onscreen keyboard for tablet PC users and mobility impaired users | <https://launchpad.net/onboard> | o | |
| app-accessibility/xvkbd | Virtual Keyboard for X Window System | <http://homepage3.nifty.com/tsato/xvkbd/> | o | gentoo repo includes this in **x11-misc** |
| app-arch/engrampa | Engrampa archive manager for MATE | <http://mate-desktop.org/> | - | dev ver. |
| app-editors/cutemarked | Qt5-based Markdown editor with live HTML preview | <http://cloose.github.io/CuteMarkEd/> | o | Fix deps. gentoo repo includes this in **app-text** |
| app-admin/browserpass | Host application required by Browserpass extension for ZX2C4's pass | <https://github.com/dannyvankooten/browserpass> | o | |
| app-admin/browserpass-bin | Host application required by Browserpass extension for ZX2C4's pass | <https://github.com/dannyvankooten/browserpass> | o | |
| app-admin/pass-import | generic importer extension for password manager ZX2C4's pass | <https://github.com/roddhjav/pass-import> | o | |
| app-admin/pass-otp | One-time password extension for password manager ZX2C4's pass | <https://github.com/tadfisher/pass-otp> | o | |
| app-editors/focuswriter | A fullscreen and distraction-free word processor | <http://gottcode.org/focuswriter/> | - | dev ver. with Qt5 support |
| app-editors/mined | Terminal-based Text Editor with extensive Unicode and CJK support | <http://towo.net/mined/> | o | |
| app-editors/juffed |  QScintilla-based tabbed text editor with syntax highlighting | <http://juffed.com/> | - | dev ver. with Qt5 support |
| app-editors/pluma | A powerful text editor for MATE | <http://mate-desktop.org/> | - | dev ver. |
| app-i18n/fcitx-kkc | Japanese libkkc module for Fcitx | <https://github.com/fcitx/fcitx-kkc> | o | |
| app-i18n/ibus-kkc | Japanese Kana Kanji input engine for IBus | <https://github.com/ueno/ibus-kkc> | o | |
| app-i18n/im-chooser | Desktop Input Method configuration tool | <https://fedorahosted.org/im-chooser/> | - | newer ver. |
| app-i18n/libkkc | Japanese Kana Kanji conversion library | <https://github.com/ueno/libkkc> | o | |
| app-i18n/libkkc-data | Japanese Kana Kanji conversion dictionary for libkkc | <https://github.com/ueno/libkkc> | o | |
| app-i18n/mozc | Mozc - Japanese Input Method | <https://github.com/google/mozc> | - | newer rev., supports app-i18n/uim |
| app-i18n/mozc-ut2 | Mozc Japanese Input Method with Additional Japanese dictionary | <http://www.geocities.jp/ep3797/mozc-ut2.html> | o | |
| app-i18n/uim | Simple, secure and flexible input method library | <http://code.google.com/p/uim/> | - | dev ver., supports Qt5 |
| app-misc/grc |  Generic Colouriser beautifies your logfiles or output of commands | <https://github.com/garabik/grc> | - | newer and dev ver. |
| app-text/aobook | Viewer for Aozora Bunko (Japanese electric library) | <http://azsky2.html.xdomain.jp/linux/aobook/index.html> | o | |
| app-text/asciidoc-gtksourceview | AsciiDoc bindings for gtksourceview | <https://launchpad.net/asciidoc-gtk-highlight> | o | |
| app-text/atril | Atril document viewer for MATE | <http://mate-desktop.org/> | - | dev ver. |
| app-text/markdown-gtksourceview | Support for Markdown in GtkSourceView and Pluma | <https://github.com/jpfleury/gedit-markdown> | o | |
| dev-libs/libmateweather | MATE library to access weather information from online services | <http://mate-desktop.org/> | - | dev ver. |
| dev-python/bbcode | A pure python bbcode parser and formatter | <https://github.com/dcwatson/bbcode> | o | improves www-apps/nikola |
| dev-python/ghp-import2 | Easily import docs to your gh-pages branch | <https://github.com/ionelmc/python-ghp-import> | o | |
| dev-python/percol | percol adds flavor of interactive selection to pipe | <https://github.com/mooz/percol> | o | |
| dev-python/phpserialize | A small library for extracting rich content from urls | <http://github.com/mitsuhiko/phpserialize> | o | improves www-apps/nikola |
| dev-python/python-caja | Python bindings for Caja file manager | <http://www.mate-desktop.org> | - | dev ver. |
| dev-python/typogrify | Filters to enhance web typography, often used with Jinja or Django | <https://github.com/mintchaos/typogrify> | o | improves www-apps/nikola |
| dev-util/meld | A graphical diff and merge tool | <http://meldmerge.org/> | - | newer ver. |
| gnome-extra/cinnamon | A fork of GNOME Shell with layout similar to GNOME 2 | <http://cinnamon.linuxmint.com/> | - | USE="pulseaudio" |
| gnome-extra/cinnamon-control-center | Cinnamon's main interface to configure various aspects of the desktop | <http://cinnamon.linuxmint.com/> | - | USE="networkmanager pulseaudio" |
| gnome-extra/cinnamon-settings-daemon | Cinnamon's settings daemon | <http://cinnamon.linuxmint.com/> | - | USE="pulseaudio" |
| mail-client/sylpheed | A lightweight email client and newsreader | <http://sylpheed.sraoss.jp/> | - | newer ver. |
| mate-base/caja | Caja file manager for the MATE desktop | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/libmatekbd | MATE keyboard configuration library | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/mate | Meta ebuild for MATE, a traditional desktop environment | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/mate-applets | Applets for the MATE Desktop and Panel | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/mate-common | Common files for development of MATE packages | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/mate-control-center | The MATE Desktop configuration tool | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/mate-desktop | Libraries for the MATE desktop that are not part of the UI | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/mate-menus | MATE menu system, implementing the F.D.O cross-desktop spec | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/mate-panel | The MATE panel | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/mate-session-manager | MATE session manager | <http://mate-desktop.org/> | - | dev ver. |
| mate-base/mate-settings-daemon | MATE Settings Daemon | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/caja-dropbox | Dropbox extension for Caja file manager | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/caja-extensions | Several extensions for Caja file manager | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/libmatemixer | Mixer library for MATE Desktop | <http://mate-desktop.org/> | o | dev ver. |
| mate-extra/mate-indicator-applet | Applet to display information from applications in the panel | <http://mate-desktop.org/> | o | |
| mate-extra/mate-media | Multimedia related programs for the MATE desktop | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/mate-polkit | A MATE specific DBUS session bus service that is used to bring up authentication dialogs | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/mate-power-manager | A session daemon for MATE that makes it easy to manage your laptop or desktop system | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/mate-screensaver | Replaces xscreensaver, integrating with the MATE desktop | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/mate-sensors-applet | MATE panel applet to display readings from hardware sensors | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/mate-system-monitor | The MATE System Monitor | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/mate-user-guide | Documents for end-users of MATE | <http://mate-desktop.org/> | o | |
| mate-extra/mate-user-share | Personal file sharing for the MATE desktop | <http://mate-desktop.org/> | - | dev ver. |
| mate-extra/mate-utils | Utilities for the MATE desktop | <http://mate-desktop.org/> | - | dev ver. |
| media-fonts/aoyagikohzan | Japanese OpenType brush font written by AOYAGI Kohzan. | <http://opentype.jp/freemouhitufont.htm> | o | |
| media-fonts/aoyagisoseki | Japanese OpenType brush font written by AOYAGI Soseki. | <http://opentype.jp/aoyagisosekifont.htm> | o | |
| media-fonts/circle-mplus | Japanese TrueType fonts based on mplus-fonts. | <http://mix-mplus-ipa.osdn.jp/> | o | |
| media-fonts/dejima-mincho | Japanese TrueType mincho font derived from Tsukiji Mincho | <https://code.google.com/p/dejima-fonts/> | o | |
| media-fonts/genju-gothic | Japanese TrueType rounded fonts based on Source Hans Sans (GennoKakuGothic) | <http://jikasei.me/font/genjyuu/> | o | |
| media-fonts/genshin-gothic | Japanese TrueType font based on Source Hans Sans (GennoKakuGothic) | <http://jikasei.me/font/genshin/> | o | |
| media-fonts/hanazono-marugothic | HZMaruGothic is a Japanese rounded gothic font based on GlyphWiki | <http://www.mars.dti.ne.jp/glyph/fonts.html> | o | |
| media-fonts/hanazono-mincho | Hanazono font is a Japanese mincho font based on GlyphWiki | <http://fonts.jp/hanazono/> | o | |
| media-fonts/hanazono-mincho-otf | HanaMinOT is a Japanese mincho OpenType font based on GryphWiki | <http://shiromoji.net/font/HanaMinOT/> | o | |
| media-fonts/hannari-mincho | a Japanese mincho OpenType font with IPAmincho kanji chars | <http://typingart.net/?p=44> | o | |
| media-fonts/honoka-mincho | a Japanese mincho OpenType font with IPAexmincho kanji chars | <http://font.gloomy.jp/honoka-mincho-dl.html> | o | |
| media-fonts/k-font | 'K-Font!', Japanese TrueType font like TV anime 'K-ON!' | <http://font.sumomo.ne.jp/index.html> | o | |
| media-fonts/kazesawa | Kazesawa Font: M+ with Source Sans Pro | <https://kazesawa.github.io/> | o | |
| media-fonts/kiloji | A Japanese hand-written TrueType font family | <http://www.ez0.net/distribution/font/kiloji/> | o | |
| media-fonts/kokoro-mincho | a Japanese mincho OpenType font with IPAmincho kanji chars | <http://typingart.net/?p=46> | o | |
| media-fonts/koku-gothic | Japanese gothic TTF based on M+ and SourceHanSans, has smaller kana | <http://freefonts.jp/font-koku-go.html> | o | |
| media-fonts/koku-mincho | Japanese mincho TTF based on IPAex, has smaller kana letter | <http://freefonts.jp/font-koku-min.html> | o | |
| media-fonts/komatuna-fonts | Komatuna fonts are modified Konatu and M+ fonts for Japanese | <http://www.geocities.jp/ep3797/modified_fonts_01.html> | o | |
| media-fonts/konatu | Japanese TrueType sans-serif font made for BeOS | <http://www.masuseki.com/?u=be/konatu.htm> | o | |
| media-fonts/koruri | Japanese TrueType font obtained by mixing M+ FONTS and Open Sans | <http://koruri.lindwurm.biz/> | - | newer ver. |
| media-fonts/meguri-fonts | Meguri fonts are modified IPA and M+ fonts for Japanese | <http://www.geocities.jp/ep3797/modified_fonts_01.html> | o | |
| media-fonts/mgenplus | Japanese TrueType font based on Source Hans Sans and M+ | <http://jikasei.me/font/mgenplus/> | o | |
| media-fonts/migu-vbd | Japanse fonts mixed mplus, IPA-font and also DejaVu, Bitter or Droid. | <http://mix-mplus-ipa.osdn.jp/migu/> | o | |
| media-fonts/mmcedar | MMCedar are combined fonts that use Motoya L Cedar and M+ fonts | <http://www.geocities.jp/ep3797/modified_fonts_01.html> | o | |
| media-fonts/monapo | combined font of ja-ipafonts (GothicP) and monafont | <http://www.geocities.jp/ep3797/modified_fonts_01.html> | o | |
| media-fonts/noto-en | Noto Fonts serif, sans and UI. an English font family by Google | <http://www.google.com/get/noto/> | o | installs EN font only |
| media-fonts/noto-cjk-jp | a Japanese OpenType font family by Google | <http://www.google.com/get/noto/cjk.html> | o | installs JA font only|
| media-fonts/pixel-mplus | Japanese TrueType monospace fonts looks like bitmap fonts, based on mplus-fonts. | <http://mix-mplus-ipa.osdn.jp/> | o | |
| media-fonts/rounded-mgenplus | Japanese TrueType rounded font based on Source Hans Sans and M+ | <http://jikasei.me/font/rounded-mgenplus/> | o | |
| media-fonts/rounded-mplus | Japanese TrueType rounded gothic fonts based on mplus-fonts | <http://jikasei.me/font/rounded-mplus/> | o | |
| media-fonts/sawarabi-fonts-otf | Sawarabi-fonts is a Japanese OTF font family | <http://osdn.jp/projects/sawarabi-fonts/> | o | |
| media-fonts/seto-font | Japanese hand-written TTF includes much more kanji chars | <http://nonty.net/sorry/> | o | |
| media-fonts/source-han-code-jp | Derivative of Source-Han-Sans replaced Latin letters to fixed | <https://github.com/adobe-fonts/source-han-code-jp/> | o | |
| media-fonts/source-han-sans | an OpenType/CFF Pan-CJK fonts by Adobe and Google. SuperOTC ver. | <https://github.com/adobe-fonts/source-han-sans> | - | installs super-OTC font |
| media-fonts/source-han-sans-jp | Subset OTF of GennoKakuGothic, a Japanese font by Adobe and Google | <https://github.com/adobe-fonts/source-han-sans> | o | installs JA font only |
| media-fonts/togoshi-font | Togoshi-font is a font family based on Kochi-alternative | <http://togoshi-font.osdn.jp/> | o | |
| media-fonts/ttedit-half | Japanese TrueType half-width mincho and gothic fonts based on IPA fonts. | <http://opentype.jp/hankakufont.htm> | o | |
| media-fonts/ttedit-twobythree | Japanese TrueType two-thirds-width mincho and gothic fonts based on IPA fonts. | <http://opentype.jp/twobythreefont.htm> | o | |
| media-fonts/umeplus-cl-fonts | UmePlus CL fonts are modified Ume CL and M+ fonts for Japanese | <http://www.geocities.jp/ep3797/modified_fonts_01.html> | o | |
| media-fonts/wlmaru | Japanese rounded TTF includes emoji, based on Wadalab Font | <http://osdn.jp/projects/jis2004/> | o | |
| media-fonts/yasashisa-antique | Japanese OpenType serif sans-serif mixed font | <http://www.fontna.com/blog/1122/> | o | |
| media-fonts/yasashisa-gothic | Japanese OpenType serif sans-serif mixed font | <http://www.fontna.com/blog/1122/> | o | |
| media-fonts/yoz-mobomoga | Japanese TrueType fonts based on mplus-fonts and ja-ipafonts. | <http://yozvox.web.fc2.com/> | o | |
| media-fonts/yozeng | Latin OpenType fonts like old typewriter and digital display, made by Y.Oz | <http://yozvox.web.fc2.com/> | o | |
| media-fonts/yozfont | Japanese OpenType pen writing fonts made by Y.Oz | <http://yozvox.web.fc2.com/> | o | |
| media-fonts/yozfontkm | Japanese OpenType brush fonts made by Y.Oz | <http://yozvox.web.fc2.com/> | o | |
| media-gfx/azdrawing | A painting software by drawing | <http://osdn.jp/projects/azdrawing/> | o | |
| media-gfx/azpainter | A full-color painting software | <http://azsky2.html.xdomain.jp/linux/azpainter/> | o | |
| media-gfx/azpainterb | A simple full-color paint software, a successor of AzPainter | <http://azsky2.html.xdomain.jp/> | o | |
| media-gfx/eom | The MATE image viewer | <http://mate-desktop.org/> | - | dev ver. |
| media-gfx/fbida | Image viewers for framebuffer and X11 | <http://www.kraxel.org/blog/linux/fbida/> | - | fix dependencies |
| media-gfx/viewnior | Fast and simple image viewer | <http://xsisqox.github.com/Viewnior/index.html> | - | newer ver. |
| media-plugins/audacious-plugins | Plugins for Audacious music player | <http://audacious-media-player.org/> | - | newer and dev ver., supports media-libs/soxr |
| media-sound/audacious | A lightweight and versatile audio player | <http://audacious-media-player.org/> | - | newer and dev ver. |
| media-sound/jack-audio-connection-kit | Jackdmp jack implemention for multi-processor machine | <http://jackaudio.org/> | - | This is a temporary pkg for [JACK2](https://github.com/jackaudio/jackaudio.github.com/wiki/Q_difference_jack1_jack2). It's compatible with JACK1. But gentoo repo names it as media-sound/jack2. Therefore, depender pkgs should depend on virtual/jack, but not fully updated |
| media-sound/yoshimi | A software synthesizer based on ZynAddSubFX | <http://yoshimi.sourceforge.net/> | - | fix to build |
| net-firewall/firewalld | Firewall daemon with D-BUS interface | <http://www.firewalld.org/> | - | newer ver. |
| sci-calculators/galculator | GTK+ based algebraic and RPN calculator | <http://galculator.mnim.org/> | - | USE="gtk2" |
| www-apps/nikola | A static website and blog generator | <https://getnikola.com/> | - | newer ver. |
| www-plugins/freshplayerplugin | PPAPI-host NPAPI-plugin adapter for flashplayer in npapi based browsers | <https://github.com/i-rinat/freshplayerplugin> | - | dev ver. |
| x11-libs/gdk-pixbuf-psd | A GdkPixbuf loader for Adobe Photoshop images | <http://cgit.sukimashita.com/gdk-pixbuf-psd.git/> | o | **UPSTEAM seems to be UNMAINTAINED**  |
| x11-libs/gdk-pixbuf-xcf | A gdk-pixbuf loader for xcf (The Gimp) files | <https://gitorious.org/xcf-pixbuf-loader/> | o | **UPSTREAM is NOT ALIVE** |
| x11-libs/libvdpau-va-gl | VDPAU driver with OpenGL/VAAPI backend | <https://github.com/i-rinat/libvdpau-va-gl> | o | |
| x11-libs/qscintilla | A Qt port of Neil Hodgson's Scintilla C++ editor class | <http://www.riverbankcomputing.com/software/qscintilla/intro> | - | New slot to build for Qt5 |
| x11-misc/lightdm | A lightweight display manager | <http://www.freedesktop.org/wiki/Software/LightDM> | - | newer ver., *if none, this is removed* |
| x11-misc/lightdm-gtk-greeter-settings | LightDM Gtk+ Greeter settings editor | <https://launchpad.net/lightdm-gtk-greeter-settings> | o | |
| x11-misc/mate-notification-daemon | MATE Notification daemon | <http://mate-desktop.org/> | - | dev ver. |
| x11-misc/mate-tweak | MATE desktop tweak tool | <https://launchpad.net/ubuntu/+source/mate-tweak> | o | |
| x11-misc/mozo | Menu editor for MATE using the freedesktop.org menu specification | <http://mate-desktop.org/> | - | dev ver. |
| x11-misc/screengrab | Qt-based program for fast creating screenshots | <https://github.com/DOOMer/screengrab> | - | dev ver. with Qt5 support |
| x11-terms/mate-terminal | The MATE Terminal Emulator | <http://mate-desktop.org/> | - | dev ver. |
| x11-themes/faience-icon-theme | A scalable icon theme called Faience | <http://tiheum.deviantart.com/art/Faience-icon-theme-255099649> | o | |
| x11-themes/gion-icon-theme | A scalable icon theme called Gion | <http://www.silvestre.com.ar/> | o | |
| x11-themes/mate-backgrounds | A set of backgrounds packaged with the MATE desktop | <http://mate-desktop.org/> | - | dev ver. |
| x11-themes/mate-icon-theme | MATE default icon themes | <http://mate-desktop.org/> | - | dev ver. |
| x11-themes/mate-icon-theme-faenza | Faenza icon theme, that was adapted for MATE desktop | <http://mate-desktop.org/> | - | dev ver. |
| x11-themes/mate-themes | A theme set for MATE Desktop Environment | <http://mate-desktop.org/> | - | dev ver. |
| x11-themes/neu-icon-theme | A scalable icon theme called Neu | <http://www.silvestre.com.ar/> | o | |
| x11-themes/smooth-themes | Themes for Smooth GTK1/GTK2 Theme Engine | <http://sourceforge.net/projects/smooth-engine/> | o | |
| x11-wm/marco | MATE default window manager | <http://mate-desktop.org/> | - | dev ver. |

----
wjn
